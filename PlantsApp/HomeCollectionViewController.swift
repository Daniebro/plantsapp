//
//  HomeCollectionViewController.swift
//  PlantsApp
//
//  Created by Danni Brito on 5/10/20.
//  Copyright © 2020 Danni Brito. All rights reserved.
//

import UIKit
import Firebase


private let reuseIdentifier = "PlantCell"

class HomeCollectionViewController: UICollectionViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    private var plantas = [Plant]()
    
    let db = Firestore.firestore()
    let user = Auth.auth().currentUser
    var ref: DocumentReference? = nil
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        title = "Mis Plantitas 😍"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addPlant))
        
        fetchPlants()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return plantas.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? PlantCollectionViewCell else {
            fatalError("unable to dequeue PlantCell.")
        }
        
        cell.plantName.text = plantas[indexPath.row].id
        
        return cell
    }
    
    @objc func addPlant(){
        
        if let vc = storyboard?.instantiateViewController(withIdentifier: "AddPlant") as? AddPlantViewController {
            navigationController?.delegate = self
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func fetchPlants() {
        db.collection("users").document(user!.uid).collection("plants").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    print(document.data())
                    self.plantas.append(Plant(
                        id: document.documentID,
                        name: document.data()["name"] as! String,
                        especie: document.data()["especie"] as! String,
                        ubicacion: document.data()["location"] as! String
                    ))
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            }
        }
    }
}
