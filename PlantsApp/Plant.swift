//
//  Plant.swift
//  PlantsApp
//
//  Created by Danni Brito on 5/10/20.
//  Copyright © 2020 Danni Brito. All rights reserved.
//

import UIKit

class Plant: NSObject {

    var id: String
    var plantName: String
    var plantEspecie: String
    var plantLocation: String
    
    init(id: String, name: String, especie: String, ubicacion: String) {
        self.id = id
        self.plantName = name
        self.plantEspecie = especie
        self.plantLocation = ubicacion
    }
    
    
}
