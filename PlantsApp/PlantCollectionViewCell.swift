//
//  PlantCollectionViewCell.swift
//  PlantsApp
//
//  Created by Danni Brito on 5/10/20.
//  Copyright © 2020 Danni Brito. All rights reserved.
//

import UIKit

class PlantCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var plantImage: UIImageView!
    @IBOutlet var plantName: UILabel!
    
}
