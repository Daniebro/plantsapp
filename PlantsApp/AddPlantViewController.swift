//
//  AddPlantViewController.swift
//  PlantsApp
//
//  Created by Danni Brito on 5/10/20.
//  Copyright © 2020 Danni Brito. All rights reserved.
//

import UIKit
import Firebase

class AddPlantViewController: UIViewController {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var locationLabel: UITextField!
    @IBOutlet var specieLabel: UITextField!
    @IBOutlet var nameLabel: UITextField!
    
    let db = Firestore.firestore()
    
    let user = Auth.auth().currentUser
    var ref: DocumentReference? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Nueva Plantita xd"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(savePlant))
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        scrollView.keyboardDismissMode = .interactive
    }
    
    @objc func savePlant(){
        ref = db.collection("users").document(user!.uid).collection("plants").addDocument(data: [
            "location": locationLabel.text ?? "",
            "name": nameLabel.text ?? "",
            "especie": specieLabel.text ?? ""
        ]) {
            err in
            if err != nil {
                print("Error adding document: \(err!.localizedDescription)")
            } else {
                print("Document added with ID: \(self.ref!.documentID)")
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            scrollView.contentInset = .zero
        } else {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom, right: 0)
        }
        
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
}
